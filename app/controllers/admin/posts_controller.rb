class Admin::PostsController < AdminsController
  before_action :authorize, only: [:index, :destroy]
  before_action :require_admin, only: [:index, :destroy]

  def index
    @posts = Post.all 
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to admin_posts_path
  end

  private

  def require_admin
    redirect_to root_path unless current_user.try(:admin?)
    flash[:success] = "You admin?"
  end
end