class AdminsController < ActionController::Base
  layout 'admin/application'

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  def authorize
    redirect_to log_in_path, alert: "Not authorized" if current_user.nil?
  end
end